(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(c-basic-offset 4)
 '(column-number-mode t)
 '(ecb-layout-name "leftright-analyse")
 '(ecb-options-version "2.40")
 '(ecb-primary-secondary-mouse-buttons (quote mouse-1--mouse-2))
 '(ecb-source-path (quote ("~/" "~/doc" "~/doc/projects" "~/down" "e:")))
 '(ecb-windows-width 0.18)
 '(inhibit-default-init nil)
 '(inhibit-startup-buffer-menu t)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice nil)
 '(initial-frame-alist (quote ((menu-bar-lines . 1) (tool-bar-lines . 1))))
 '(initial-major-mode (quote text-mode))
 '(initial-scratch-message nil)
 '(lisp-body-indent 2)
 '(make-backup-files nil)
 '(package-selected-packages (quote (auctex)))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(speedbar-after-create-hook (quote (speedbar-frame-reposition-smartly)))
 '(speedbar-before-visiting-file-hook nil)
 '(speedbar-before-visiting-tag-hook nil)
 '(speedbar-show-unknown-files t)
 '(tab-always-indent t)
 '(tool-bar-mode nil))
 
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 100 :width normal :foundry "outline" :family "WenQuanYi Micro Hei Mono"))))
 '(fixed-pitch ((t (:family "WenQuanYi Micro Hei Mono"))))
 '(mode-line ((((class color) (min-colors 88)) (:background "grey75" :foreground "black" :box (:line-width -1 :style released-button) :height 0.9 :family "WenQuanYi Micro Hei"))))
 '(variable-pitch ((t (:family "WenQuanYi Micro Hei")))))


;--------------------global settings--------------------
(mouse-avoidance-mode 'animate)
;(global-set-key (kbd "<mouse-9>") 'previous-buffer)
;(global-set-key (kbd "<mouse-8>") 'next-buffer)
(global-set-key (kbd "<f5>") 'speedbar)
(global-set-key (kbd "<f8>") 'calendar)
(global-set-key (kbd "<f9>") 'shell)
(global-set-key (kbd "<f10>") 'compile)
(global-set-key (kbd "<f11>") 'auto-complete-mode)
(global-set-key (kbd "<f12>") 'eassist-switch-h-cpp)
(global-set-key (kbd "C-.") 'semantic-ia-complete-symbol-menu)
;(global-set-key (kbd "<C-SPC>") nil)
;(global-set-key (kbd "<S-SPC>") 'set-mark-command)
;(add-to-list 'load-path "~/.emacs.d")

;; Use package.el to manage packages
(require 'package)
(package-initialize)

;; CC mode 设置
(c-set-offset 'inline-open 0)
(c-set-offset 'friend '-)
(c-set-offset 'substatement-open 0)

;; 启动窗口大小
(setq default-frame-alist
      '((height . 60) (width . 160))) 
(setq-default line-spacing 2)
;(setq inhibit-startup-message t)
;(setq inhibit-startup-screen t)

;; Color theme
(load-theme 'misterioso)
(setq frame-background-mode 'dark)

;;最大化设置
(defun my-maximized ()
  (interactive)
  (x-send-client-message
   nil 0 nil "_NET_WM_STATE" 32
   '(1 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
  (interactive)
  (x-send-client-message
   nil 0 nil "_NET_WM_STATE" 32
   '(1 "_NET_WM_STATE_MAXIMIZED_VERT" 0)))

(defun toggle-fullscreen ()
  "toggles whether the currently selected frame consumes the entire display
or is decorated with a window border"
  (interactive)
  (let ((f (selected-frame)))
    (modify-frame-parameters 
     f
     `((fullscreen . ,(if (eq nil (frame-parameter f 'fullscreen)) 
                          'fullboth
                        nil))))))

(global-set-key (kbd "<f6>") 'ecb-activate)
(global-set-key (kbd "<S-f6>") 'ecb-deactivate)
;(global-set-key (kbd "<f7>") 'my-maximized)
(global-set-key (kbd "<f7>") 'toggle-fullscreen)

;;允许emacs和外部其他程序的粘贴
(setq x-select-enable-clipboard t)
(setq mouse-yank-at-point nil)

;;启动语法高亮模式
(global-font-lock-mode t)

;;在标题栏显示buffer的名字
(setq frame-title-format "emacs@%b")

;;去掉工具栏
(tool-bar-mode nil)

;;使用gdb的图形模式
(setq gdb-many-windows t)

;;把.h关联到c++-mode
(setq auto-mode-alist
      (append '(("\\.h$" . c++-mode)) auto-mode-alist))

;;把.m关联到octave-mode
(setq auto-mode-alist
      (append '(("\\.m$" . octave-mode)) auto-mode-alist))

;;退出gdb,term,compilation时关闭对应的buffer 
;;关闭函数
(defun kill-buffer-when-shell-command-exit () 
  "Close current buffer when `shell-command' exit." 
  (let ((process (ignore-errors (get-buffer-process (current-buffer))))) 
    (when process 
      (set-process-sentinel process 
			    (lambda (proc change) 
			      (when (string-match "\\(finished\\|exited\\)" change) 
				(kill-buffer (process-buffer proc)))))))) 

(add-hook 'gdb-mode-hook 'kill-buffer-when-shell-command-exit)  
(add-hook 'term-mode-hook 'kill-buffer-when-shell-command-exit) 
(defun kill-buffer-when-compile-success (process) 
  "Close current buffer when `shell-command' exit." 
  (set-process-sentinel process 
			(lambda (proc change) 
			  (when (string-match "finished" change) 
			    (delete-windows-on (process-buffer proc)))))) 
;;编译成功后自动关闭"compilation* buffer
(add-hook 'compilation-start-hook 'kill-buffer-when-compile-success)


;---------------------auto complete-------------------
(add-to-list 'load-path "~/.emacs.d/auto-complete")
(require 'auto-complete)
(require 'auto-complete-config)

(add-to-list 'ac-dictionary-directories "~/.emacs.d/auto-complete/ac-dict")
(ac-config-default)

;; Show 0.8 second later
(setq ac-auto-show-menu 0.5)

;; 20 lines
(setq ac-menu-height 20)

;; Examples
(set-face-background 'ac-candidate-face "lightgray")
(set-face-underline 'ac-candidate-face "darkgray")
(set-face-background 'ac-selection-face "steelblue")

(add-hook 'c++-mode (lambda () (add-to-list 'ac-sources 'ac-source-semantic)))

;; Complete member name by C-c . for C++ mode.
(add-hook 'c++-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c .") 'ac-complete-semantic)))

;; Complete file name by C-c /
;(global-set-key (kbd "C-c /") 'ac-complete-filename)

(defun semantic-and-gtags-complete ()
  (interactive)
  (auto-complete '(ac-source-semantic ac-source-gtags)))

(defun auto-complete-settings ()
  "Settings for `auto-complete'."
  ;; After do this, isearch any string, M-: (match-data) always
  ;; return the list whose elements is integer
  (global-auto-complete-mode 1)
 
  ;; 不让回车的时候执行`ac-complete', 因为当你输入完一个
  ;; 单词的时候, 很有可能补全菜单还在, 这时候你要回车的话,
  ;; 必须要干掉补全菜单, 很麻烦, 用M-j来执行`ac-complete'

  ;(define-key ac-complete-mode-map "<return>"   'nil)
  ;(define-key ac-complete-mode-map "RET"        'nil)
  ;(define-key ac-complete-mode-map "M-j"        'ac-complete)
  ;(define-key ac-complete-mode-map "<C-return>" 'ac-complete)
  (define-key ac-complete-mode-map "\C-n"        'ac-next)
  (define-key ac-complete-mode-map "\C-p"        'ac-previous)
 
  (setq ac-dwim t)
  (setq ac-candidate-max ac-candidate-menu-height)
 
  (set-default 'ac-sources
               '(ac-source-semantic
                 ac-source-yasnippet
                 ac-source-abbrev
                 ac-source-words-in-buffer
                 ac-source-words-in-all-buffer
                 ac-source-imenu
                 ac-source-files-in-current-dir
                 ac-source-filename))
;  (setq ac-modes ac+-modes)
 
  (dolist (command `(backward-delete-char-untabify delete-backward-char))
    (add-to-list 'ac-trigger-commands command))
 
  (defun ac-start-use-sources (sources)
    (interactive)
    (let ((ac-sources sources))
      (call-interactively 'ac-start)))
 
  (defvar ac-trigger-edit-commands
    `(self-insert-command
      delete-backward-char
      backward-delete-char
      backward-delete-char-untabify)
    "*Trigger edit commands that specify whether `auto-complete' should start or not when `ac-completing'."))
 
(eval-after-load "auto-complete"
  '(auto-complete-settings))
 
(eval-after-load "cc-mode"
  '(progn
     (dolist (command `(c-electric-backspace
                        c-electric-backspace-kill))
       (add-to-list 'ac-trigger-commands command)
       (add-to-list 'ac-trigger-edit-commands command))))
 
(eval-after-load "autopair"
  '(progn
     (dolist (command `(autopair-insert-or-skip-quote
                        autopair-backspace
                        autopair-extra-skip-close-maybe))
       (add-to-list 'ac-trigger-commands command))
 
     (defun ac-trigger-command-p ()
       "Return non-nil if `this-command' is a trigger command."
       (or
        (and
         (memq this-command ac-trigger-commands)
         (let* ((autopair-emulation-alist nil)
                (key (this-single-command-keys))
                (beyond-autopair (or (key-binding key)
                                     (key-binding (lookup-key local-function-key-map key)))))
           (memq beyond-autopair ac-trigger-edit-commands)))
        (and ac-completing
             (memq this-command ac-trigger-edit-commands))))))
 
(defun ac-settings-4-lisp ()
  "Auto complete settings for lisp mode."
  (setq ac-omni-completion-sources '(("\\<featurep\s+'" ac+-source-elisp-features)
                                     ("\\<require\s+'"  ac+-source-elisp-features)
                                     ("\\<load\s+\""    ac-source-emacs-lisp-features)))
  (ac+-apply-source-elisp-faces)
  (setq ac-sources
        '(ac-source-yasnippet
          ac-source-symbols
          ;; ac-source-semantic
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ;; ac-source-imenu
          ac-source-files-in-current-dir
          ac-source-filename)))
 
(defun ac-settings-4-java ()
  (setq ac-omni-completion-sources (list (cons "\\." '(ac-source-semantic))
                                         (cons "->" '(ac-source-semantic))))
  (setq ac-sources
        '(;;ac-source-semantic
          ac-source-yasnippet
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename)))
 
(defun ac-settings-4-c ()
  (setq ac-omni-completion-sources (list (cons "\\." '(ac-source-semantic))
                                         (cons "->" '(ac-source-semantic))))
  (setq ac-sources
        '(ac-source-yasnippet
          ac-source-c-keywords
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename)))
 
(defun ac-settings-4-cpp ()
  (setq ac-omni-completion-sources
        (list (cons "\\." '(ac-source-semantic))
              (cons "->" '(ac-source-semantic))))
  (setq ac-sources
        '(ac-source-yasnippet
          ac-source-c++-keywords
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename)))
 
(defun ac-settings-4-text ()
  (setq ac-sources
        '(;;ac-source-semantic
          ac-source-yasnippet
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-imenu)))
 
(defun ac-settings-4-eshell ()
  (setq ac-sources
        '(;;ac-source-semantic
          ac-source-yasnippet
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename
          ac-source-symbols
          ac-source-imenu)))
 
(defun ac-settings-4-ruby ()
  (require 'rcodetools-settings)
  (setq ac-omni-completion-sources
        (list (cons "\\." '(ac-source-rcodetools))
              (cons "::" '(ac-source-rcodetools)))))
 
(defun ac-settings-4-html ()
  (setq ac-sources
        '(;;ac-source-semantic
          ac-source-yasnippet
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename)))
 
(defun ac-settings-4-tcl ()
  (setq ac-sources
        '(;;ac-source-semantic
          ac-source-yasnippet
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename)))
 
(defun ac-settings-4-awk ()
  (setq ac-sources
        '(;;ac-source-semantic
          ac-source-yasnippet
          ac-source-abbrev
          ac-source-words-in-buffer
          ac-source-words-in-all-buffer
          ac-source-files-in-current-dir
          ac-source-filename)))

(defun ac-semantic-candidate (prefix)
  (if (memq major-mode
            '(c-mode c++-mode jde-mode java-mode))
      (mapcar 'semantic-tag-name
              (ignore-errors
                (or (semantic-ia-get-completions
                     (semantic-analyze-current-context) (point))
                    (senator-find-tag-for-completion (regexp-quote prefix)))))))

;; (am-add-hooks
;;  `(lisp-mode-hook emacs-lisp-mode-hook lisp-interaction-mode-hook
;;                   svn-log-edit-mode-hook change-log-mode-hook)
;;  'ac-settings-4-lisp)

;; (apply-args-list-to-fun
;;  (lambda (hook fun)
;;    (am-add-hooks hook fun))
;;  `(('java-mode-hook   'ac-settings-4-java)
;;    ('c-mode-hook      'ac-settings-4-c)
;;    ('c++-mode-hook    'ac-settings-4-cpp)
;;    ('text-mode-hook   'ac-settings-4-text)
;;    ('eshell-mode-hook 'ac-settings-4-eshell)
;;    ('ruby-mode-hook   'ac-settings-4-ruby)
;;    ('html-mode-hook   'ac-settings-4-html)
;;    ('java-mode-hook   'ac-settings-4-java)
;;    ('awk-mode-hook    'ac-settings-4-awk)
;;    ('tcl-mode-hook    'ac-settings-4-tcl)))
 
;(eal-eval-by-modes
; ac-modes
; (lambda (mode)
;   (let ((mode-name (symbol-name mode)))
;     (when (and (intern-soft mode-name) (intern-soft (concat mode-name "-map")))
;       (define-key (symbol-value (am-intern mode-name "-map")) (kbd "C-c a") 'ac-start)))))
 
(provide 'auto-complete-settings)
(require 'auto-complete-settings)
;---------------------auto complete-------------------

;---------------------日程安排设置--------------------
;(setq todo-file-do "~/.emacsfile/todo/do")
;(setq todo-file-done "~/.emacsfile/todo/done")
;(setq todo-file-top "~/.emacsfile/todo/top")
;(setq appt-issue-message t)

;; 让emacs能计算日出日落的时间，在 calendar 上用 S 即可看到
(setq calendar-latitude +39.93)
(setq calendar-longitude +116.28)
(setq calendar-location-name "北京")

;; 设置阴历显示，在 calendar 上用 pC 显示阴历
(setq chinese-calendar-celestial-stem
  ["甲" "乙" "丙" "丁" "戊" "己" "庚" "辛" "壬" "癸"])
(setq chinese-calendar-terrestrial-branch
  ["子" "丑" "寅" "卯" "辰" "巳" "戊" "未" "申" "酉" "戌" "亥"])

(setq mark-diary-entries-in-calendar t)     ; 标记calendar上有diary的日期

(setq view-calendar-holidays-initially t) ; 打开calendar的时候显示节日 

;; 去掉不关心的节日，设定自己在意的节日，在 calendar 上用 h 显示节日
(setq christian-holidays nil)
(setq hebrew-holidays nil)
(setq islamic-holidays nil)
(setq solar-holidays nil)
(setq general-holidays '((holiday-fixed 1 1 "元旦")
                         (holiday-fixed 2 14 "情人节")
                         (holiday-fixed 3 14 "白色情人节")
                         (holiday-fixed 4 1 "愚人节")
                         (holiday-fixed 5 1 "劳动节")
                         (holiday-float 5 0 2 "母亲节")
                         (holiday-fixed 6 1 "儿童节")
                         (holiday-float 6 0 3 "父亲节")
                         (holiday-fixed 7 1 "建党节")
                         (holiday-fixed 8 1 "建军节")
                         (holiday-fixed 9 10 "教师节")
                         (holiday-fixed 10 1 "国庆节")
                         (holiday-fixed 12 25 "圣诞节")))
(setq local-holidays
      '((holiday-chinese 1 15 "元宵节 (正月十五)")
	(holiday-chinese 5  5 "端午节 (五月初五)")
	(holiday-chinese 9  9 "重阳节 (九月九)")
	(holiday-chinese 8 15 "中秋节 (八月十五)")
	(holiday-chinese 6 4 "妈妈生日 (六月初四)")
	(holiday-chinese 4 26 "爸爸生日 (四月二十六)")
	(holiday-chinese 12 6 "秀生日 (腊月初六)")
	(holiday-chinese 12 16 "姐生日 (腊月十六)")
	(holiday-fixed 6 20 "勉勉生日")
	(holiday-fixed 9 25 "恋爱纪念日")
	(holiday-fixed 10 26 "求婚纪念日")
	(holiday-fixed 12 16 "结婚纪念日")))

(setq diary-file "~/.emacsfile/diary/diary")
(setq diary-mail-addr "aaronlau33@gmail.com")
(add-hook 'diary-hook 'appt-make-list)


;--------------------Template--------------------
;(add-to-list 'load-path "~/.emacs.d/template/lisp")
;(require 'template)
;(template-initialize)

;--------------------AucTeX--------------------
(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook #'LaTeX-install-toolbar)
(add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

(setq TeX-output-view-style (quote (("^pdf$" "." "evince %o %(outpage)"))))

(add-hook 'LaTeX-mode-hook
	  (lambda()
	    (add-to-list 'TeX-command-list '("XeLaTeX" "%`xelatex%(mode)%' %t" TeX-run-TeX nil t))
	    (setq TeX-command-default "XeLaTeX")
	    (setq TeX-show-compilation t)
	    ))



;--------------------CEDET--------------------
;(add-to-list 'load-path "~/.emacs.d/cedet-1.1/common")
(require 'cedet)
(require 'semantic/ia)
;(require 'semantic-gcc)
;(load-file "../lisp/cedet/cedet.el")

;; Enable EDE (Project Management) features
(global-ede-mode t)

;; Semantic features
;(semantic-load-enable-minimum-features)

 
;; (semantic-load-enable-excessive-code-helpers)
;(semantic-load-enable-code-helpers)
;(semantic-load-enable-semantic-debugging-helpers)

(global-semantic-idle-completions-mode 1)
(global-semantic-show-parser-state-mode 1)
(global-semanticdb-minor-mode 1)
(global-semantic-idle-summary-mode 1)
;(global-senator-minor-mode 1)
(global-semantic-decoration-mode 1)
;global-semantic-mru-bookmark-mode
 
;; Enable SRecode (Template management) minor-mode.
;(global-srecode-minor-mode 1)

;; Switch between h/cpp
;; which is already bind to <f12>
(require 'eassist nil 'noerror)
(setq eassist-header-switches
      '(("h" . ("cpp" "cxx" "c++" "CC" "cc" "C" "c" "mm" "m"))
        ("hh" . ("cc" "CC" "cpp" "cxx" "c++" "C"))
        ("hpp" . ("cpp" "cxx" "c++" "cc" "CC" "C"))
        ("hxx" . ("cxx" "cpp" "c++" "cc" "CC" "C"))
        ("h++" . ("c++" "cpp" "cxx" "cc" "CC" "C"))
        ("H" . ("C" "CC" "cc" "cpp" "cxx" "c++" "mm" "m"))
        ("HH" . ("CC" "cc" "C" "cpp" "cxx" "c++"))
        ("cpp" . ("hpp" "hxx" "h++" "HH" "hh" "H" "h"))
        ("cxx" . ("hxx" "hpp" "h++" "HH" "hh" "H" "h"))
        ("c++" . ("h++" "hpp" "hxx" "HH" "hh" "H" "h"))
        ("CC" . ("HH" "hh" "hpp" "hxx" "h++" "H" "h"))
        ("cc" . ("hh" "HH" "hpp" "hxx" "h++" "H" "h"))
        ("C" . ("hpp" "hxx" "h++" "HH" "hh" "H" "h"))
        ("c" . ("h"))
        ("m" . ("h"))
        ("mm" . ("h"))))

;; Code folding
;(require 'semantic-tag-folding nil 'noerror)
;(global-semantic-tag-folding-mode 1)
;(define-key semantic-tag-folding-mode-map (kbd "C-c -") 'semantic-tag-folding-fold-block)
;(define-key semantic-tag-folding-mode-map (kbd "C-c =") 'semantic-tag-folding-show-block)
;(define-key semantic-tag-folding-mode-map (kbd "C-_") 'semantic-tag-folding-fold-all)
;(define-key semantic-tag-folding-mode-map (kbd "C-+") 'semantic-tag-folding-show-all)
;(global-set-key (kbd "C->") 'global-semantic-tag-folding-mode)



;--------------------ECB--------------------
(setq stack-trace-on-error t)
(require 'semantic/analyze)
(provide 'semantic-analyze)
(provide 'semantic-ctxt)
(provide 'semanticdb)
(provide 'semanticdb-find)
(provide 'semanticdb-mode)
(provide 'semantic-load)

(add-to-list 'load-path "~/.emacs.d/ecb-2.40")
(require 'ecb-autoloads)

;;Why are the lines in the ECB-, temp- and compilation-buffers 
;;  not wrapped but truncated?
;;Check the variable truncate-partial-width-windows and set it to nil. 
 (setq truncate-partial-width-windows nil)

(setq ecb-auto-activate t
           ecb-tip-of-the-day nil
           ecb-tree-indent 4
           ;ecb-windows-height 0.5
           ;ecb-windows-width 0.20
	   ecb-auto-compatibility-check nil
	   ecb-version-check nil)

(add-hook 'window-setup-hook (lambda () (tool-bar-mode -1)))

;-----------------Markdown-mode--------------------
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
